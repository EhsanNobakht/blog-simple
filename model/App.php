<?php


class App
{
    public static function handleByController($controllerName)
    {
        $controllerName = "{$controllerName}Controller";
        require_once __DIR__ . "/../controller/BaseController.php";
        require_once __DIR__ . "/../controller/{$controllerName}.php";
        $controller = new $controllerName();
    }
}
