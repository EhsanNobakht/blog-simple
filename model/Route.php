<?php

class Route
{
    protected static $instance;
    protected $routes;

    public function __construct()
    {
        $base_url = self::getCurrentUri();
        $base_url = trim($base_url, '/ ');
        $routes = array();
        $routes_parts = explode('/', $base_url);
        foreach ($routes_parts as $route) {
            if (trim($route) != '') {
                array_push($routes, $route);
            }
        }

        $this->routes = $routes_parts;
        self::$instance = $this;
    }

    public function fetchCurrentRoute()
    {
        return array_shift($this->routes);
    }

    public function getRoutes()
    {
        return $this->routes;
    }

    private static function getCurrentURI()
    {
        $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
        $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }

        $uri = '/' . trim($uri, '/');
        return $uri;
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            new Route();
        }
        return self::$instance;
    }

}
