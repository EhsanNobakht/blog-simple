<?php


$route = Route::getInstance();
$currentRoute = $route->fetchCurrentRoute();

switch (strtolower($currentRoute)) {
    case NULL:
        echo "Home Page";
        break;
    case 'blog':
        App::handleByController(ucfirst(strtolower($currentRoute)));
        break;
    default:
        echo "Not Found?";
        break;
}