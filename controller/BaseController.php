<?php

class BaseController
{
    public function __construct() {
        $route = Route::getInstance();
        $currenRoute = $route->fetchCurrentRoute();
        if ($currenRoute == NULL) {
            $currenRoute = 'index';
        }
        if (method_exists($this, $currenRoute)) {
            $this->currentRoute();
        } else {
            echo "Page Not Found";
        }
    }

}